---
title: "De quoi la pluralité des termes pour parler des recherches et sciences participatives est-elle le nom ?"
subtitle: "Séminaire annuel du LPED -- 28 mars 2024"
author: "Aymeric Luneau"
institute: "médialab, Sciences Po Paris"
date: "`r format(Sys.time(), '%d %B %Y')`"
bibliography: ma_bib.bib
output:
  revealjs::revealjs_presentation:
    theme:  "serif" #"default", "dark", "simple", "sky", "beige", "serif",  "blood", "moon", "night", "black", "league", "white"
    highlight: haddock
    center: true
    transition: slide
    self_contained: false
    number_sections: true
    toc: true
    toc_depth: 3
    reveal_plugins: ["notes", "menu", "chalkboard"]
    reveal_options:
      slideNumber: true
      previewLinks: true
      controlsTutorial: true
    template-partials:
        - title-slide.html
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	echo = FALSE,
	message = FALSE,
	warning = FALSE,
	fig.align = "center"
)
options(encoding = 'UTF-8')




library(tidyverse)
library(igraph)
library(questionr)
library(ggpubr)
library(knitr)
library(kableExtra)
library(dplyr)
library(scales)
library(DT)
library(tidygraph)
library(ggraph)
library(stringr)
library(bookdown)
library(flextable)
library(officer)
#library("ForceAtlas2")
library(RColorBrewer)
library(bookdown)
library(FactoMineR)
library(ggrepel)

```

```{css}

h2.chapeau{
text-align: center;
fonwt-weight : normal;
line-height: 90px;
}

h1.title {
font-size: 10px;
line-height: 90px;
}

h1.subtitle {
font-size: 8px;
line-height: 90px;
}

h2.author {
  font-weight: normal;
  font-size: 8px;
  line-height: 90px;
}

h2.institute {
  font-weight: normal;
  line-height: 90px;
}

h3.date {
  font-weight: normal;
  line-height: 90px;
}

 .reveal .slide {
    text-align: left;
  }
   .reveal h1 {
    font-size: 34pt;
    text-transform: none;
  }
  .reveal h2 {
    font-size: 30pt;
    text-transform: none;
  }
  .reveal p {
    text-align: left;
    font-size: 24pt;
  }
  .reveal ul {
    display: block;
  }
  .reveal ol {
    display: block;
  }
  .reveal section img { background:none; border:none; box-shadow:none;
  }



}
   .reveal cite{
    font-size: 12pt;
  }

   .reveal refs{
    font-size: 0.5em;
    column-count: 2;
  }

  .reveal slide level2 unnumbered{
  font-size: 0.5em;
  }

  .reveal csl-entry{
    font-size: 12pt;
  }

  .twocolumn{
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 10px;
  text-align: left;
  }

  .math span {
        font-size: 24pt;
        text-align: left;
      }

  .reveal table {
    outline : 0;
    border: 1px solid;
    padding-left: 0;
    border-collapse: collapse;
    border-spacing: 0;
  }
   .reveal table th {
    border: 1px solid;
    font-size: 24pt;
    vertical-align: top;
  }
   .reveal table tr{
    border: 1px solid;
    vertical-align: top;
  }
  .reveal table td{
    border: 1px solid;
    font-size:22pt;
    vertical-align: top;
    padding-left: 1;
    padding-right: 1;
  }
  .reveal td:hover {background-color: coral;}
  .reveal th:hover {background-color: coral;}
  
  .gallery {
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(8, 5vw);
  grid-gap: 1rem; 
}

.gallery__img {
  width: 10%;
  height: 10%;
  object-fit: cover;
  display: block; 
  filter: grayscale(100%);
}

.gallery__item--1 {
  grid-column-start: 1;
  grid-column-end: 2;
  grid-row-start: 1;
  grid-row-end: 2;

  /** Alternative Syntax **/
  /* grid-column: 1 / span 2;  */
  /* grid-row: 1 / span 2; */
}

.gallery__item--2 {
  grid-column-start: 2;
  grid-column-end: 4;
  grid-row-start: 1;
  grid-row-end: 2;
}

.gallery__item--3 {
  grid-column-start: 4;
  grid-column-end: 6;
  grid-row-start: 1;
  grid-row-end: 2;

  /** Alternative Syntax **/
  /* grid-column: 5 / span 4;
  grid-row: 1 / span 5; */
}

.gallery__item--4 {
  grid-column-start: 1;
  grid-column-end: 3;
  grid-row-start: 3;
  grid-row-end: 6;

  /** Alternative Syntax **/
  /* grid-column: 1 / span 4;  */
  /* grid-row: 3 / span 3; */
}

.gallery__item--5 {
  grid-column-start: 3;
  grid-column-end: 6;
  grid-row-start: 3;
  grid-row-end: 6;

  /** Alternative Syntax **/
  /* grid-column: 1 / span 4; */
  /* grid-row: 6 / span 3; */
}

.gallery__item--6 {
  grid-column-start: 6;
  grid-column-end: 9;
  grid-row-start: 3;
  grid-row-end: 6;

  /** Alternative Syntax **/
  /* grid-column: 5 / span 4; */
  /* grid-row: 6 / span 3; */
}

.wrap {
  position: relative;
}

.wrap .verso {
  position: absolute;
  left: 0;
  top: 0;
  visibility: hidden;
  opacity: 0;
  transition: visibility 0s, opacity 0.3s linear;
}

.wrap:hover .verso {
  visibility: visible;
  opacity: 1;
}

.wrap:hover .recto {
  visibility: hidden;
  opacity: 0;
}


.animate-text { 
	visibility:hidden;
  font-family:"courier",sans-serif;
  color:black; 
}


.animate-text { 
  width:1000px; /* pour visualiser le passage a la ligne */
  border:1px dashed #ccc;
}

```

```{js}
"use strict";
window.addEventListener("DOMContentLoaded", (event) => {
  animate_text();
});
// -------------------
function animate_text()
{
  let delay = 50,
      delay_start = 0,
      contents,
      letters;

  document.querySelectorAll(".animate-text").forEach(function (elem) {
    contents = elem.textContent.trim();
    elem.textContent = "";
    letters = contents.split("");
    elem.style.visibility = 'visible';

    letters.forEach(function (letter, index_1) {
      setTimeout(function () {
        // ---------
        // effet machine à écrire (SIMPLE)
        elem.textContent += letter;
        // ---------
        // OU :
        // effet machine à écrire + animation CSS (SPECIAL)
        /*
        var span = document.createElement('span');
        span.innerHTML = letter.replace(/ /,'&nbsp;');
        elem.appendChild(span);
*/
        // ---------
      }, delay_start + delay * index_1);
    });
    delay_start += delay * letters.length;
  });
}


```

<a href="http://aymericluneau.frama.io/presentation">http://aymericluneau.frama.io/presentation</a>

# Enjeu des mots

<p class="animate-text">

"Et j'aurais tendance à ne pas l'appeler recherche participative, mais recherche collaborative."

</p>

<p class="animate-text">

"Qui dit recherche interventionnelle, dit souvent recherche interventionnelle co-construite avec les acteurs et donc participative."

</p>

<p class="animate-text">

"Nous, le terme qu'on utilise, c'est : recherche partenariale."

</p>

## Une petite illustration

> Recherches participatives: connaissances et reconnaissances (Colloque de Caen, novembre 2023)

> Qu'apportent les sciences participatives aux pratiques de la recherche (séminaire 2024 du Centre d'Alembert)

> Les sciences citoyennes au LPED, Kézaco ? (séminaire annuel du LPED)

## L'approche taxonomique

La première approche possible pour affronter cette pluralité est l'approche taxonomique: on va classer les différentes pratiques en fonction de leurs points communs et leurs différences.

-   Les projets définis et pilotés par une institution académique dans lesquels la participation se limite pour l'essentiel à collecter des données.

-   Les projets conduits conjointement par différentes parties prenantes afin de résoudre un problème collectif

## L'approche sociologique

Considérer les termes comme le résultat de configurations sociales qu'il s'agit d'analyser.

-   Enjeu scientifique : comprendre comment les pratiques de recherche se transforment

-   Enjeu "normatif" : reconnaître et défendre un pluralisme épistémique au sein du champ des recherches et sciences participatives.

## Une sociologie politique et pragmatique des "sciences citoyennes", des "recherches participatives", etc.

**Sociologie politique** : Analyser les configurations d'acteurs qui influent sur la manière de concevoir les sciences ou les recherches participatives [@demortain2019; @frickel2006]

**Sociologie pragmatique** : saisir les problèmes que les acteurs (chercheurs et non-chercheurs) cherchent à résoudre à travers le déploiement des "sciences citoyennes".

# La recherche participative au pluriel
